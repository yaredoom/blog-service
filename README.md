# README

A Consumer blog-service for a Contract Testing POC
A consumers GET /users/{id} api from the user-service


# BlogService

This application manages blog articles.
Since the only goal of this application is to demonstrate the usage of
contract testing with pact.io, only a show endpoint will be implemented.

An article belongs to a user, but the user data is stored on the user profile application. In order to show the user name and email, this application makes a rest call to the user profile application.

# Create and publish Contract using Pact

This application relies on one contract with the user profile application to retrieve the data related to a post author.

The contract is specified in a spec file, which is also used to stub the rest call which would be made to the external service

```
let(:response_body) do
    {
        'name': 'Contract Tester',
        'email': 'Contract.TESTER@example.com'
    }
    end

    before do
      user_service.given('a user with id 100 exists')
                  .upon_receiving('a request for user 100')
                  .with(method: :get, path: '/users/100', query: '')
                  .will_respond_with(
                    status: 200,
                    # headers: { 'Content-Type' => 'application/json'},
                    body: response_body
                  )
    end

    it { is_expected.to be_kind_of Remote::User }
    its(:name) { is_expected.to eq('contract tester') }
    its(:email) { is_expected.to eq('contract.tester@example.com') }
```

## Run Rspecs

From the consumer-service, how do I publish a contract?
 Run Rspecs
 Running the Consumer Contract Test: - This will generate the Pact Contract file

`bundle exec rspec spec/models/remote/user_spec.rb  --format documentation`

After running your specs, a contract json will be automatically generated, based on your spec, and it will be stored in `spec/pacts`

## Publishing the contract to a pact broker

At this point you want to publish the new contract to a pact broker, so that the
provider can use it to verify that his API complies to it.

To publish the contract run

```
bundle exec rails pact:publish
```

The `pact:publish` command will invoke the below rake task;

```
/lib/tasks/pact_broker_client_publication_task.rake
```

